<!-- FOOTER -->
<footer class="footer-page" <?php echo lazyload('images/Contact-BG.png','1912','544','style');?>>
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <div class="footer-thumb">
                              <img class="wow fadeInUp logo-web-cii3-footer" data-wow-delay="0.4s" <?php echo lazyload('images/Web-logo-ขาว.png','198');?> alt="Healthy Vaga"
                              aria-label="Healthy Vaga">
                              <h4 class="wow fadeInUp title-h1" data-wow-delay="0.6s">บริษัท เฮลตี เวก้า จำกัด</h4>

                              <div class="contact-info">
                                   <p class="wow fadeInUp" data-wow-delay="0.8s"><i class="fa fa-map-pin"></i> 298/1 หมู่ 5 ต.ปากเกร็ด อ.ปากเกร็ด จ.นนทบุรี 11120</p>
                                   <p class="wow fadeInUp" data-wow-delay="1s"><i class="fa fa-phone"></i> 092-496-4242</p>
                                   <p class="wow fadeInUp" data-wow-delay="1.2s"><i class="fa fa-envelope-o"></i> subpanich2520rich<i class="fa fa-at"></i>gmail.com</p>
                              </div>

                              <ul class="social-icon wow fadeInUp" data-wow-delay="1.4s">
                                   <li><a href="#" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                                   <li><a href="#" class="fa fa-twitter"></a></li>
                                   <li><a href="#" class="fa fa-instagram"></a></li>
                              </ul>
                         </div>
                    </div>

                    <div class="col-md-3 col-sm-12 footer-link">
                         <h4 class="wow fadeInUp" data-wow-delay="1.6s">ลิงค์ที่เกี่ยวข้อง</h4>
                         <p class="wow fadeInUp" data-wow-delay="1.8s"><a href="<?php echo BASE_URL;?>about.html"><i class="fa fa-angle-right"></i> เกี่ยวกับเรา</a></p>
                         <p class="wow fadeInUp" data-wow-delay="1.9s"><a href="<?php echo BASE_URL;?>service.html"><i class="fa fa-angle-right"></i> บริการ</a></p>
                         <p class="wow fadeInUp" data-wow-delay="2s"><a href="<?php echo BASE_URL;?>product.html"><i class="fa fa-angle-right"></i> ผลิตภัณฑ์</a></p>
                         <p class="wow fadeInUp" data-wow-delay="2.1s"><a href="<?php echo BASE_URL;?>blog.html"><i class="fa fa-angle-right"></i> ข่าวสารและกิจกรรม</a></p>
                         <!-- <p class="wow fadeInUp" data-wow-delay="2.2s"><a href="#"><i class="fa fa-angle-right"></i> สมัครตัวแทนจำหน่าย</a></p> -->
                         <!-- <p class="wow fadeInUp" data-wow-delay="2.3s"><a href="#"><i class="fa fa-angle-right"></i> แผนผังเว็บไชต์</a></p> -->
                    </div>

                    <div class="col-md-3 col-sm-12 footer-link">
                         <h4 class="wow fadeInUp" data-wow-delay="1.8s">รองรับมาตราฐานสากล</h4>
                         <p class="wow fadeInUp" data-wow-delay="1.8s"><a href="#"><i class="fa fa-angle-right"></i> มาตราฐาน GMP (Good Manufacturing Practice)</a></p>
                         <p class="wow fadeInUp" data-wow-delay="1.9s"><a href="#"><i class="fa fa-angle-right"></i> มาตราฐาน HACCP (Heard Analysis Citical Control Poin)</a></p>
                    </div>
               </div>
               <div class="angle-up-btn">
                    <a href="#top" class="smoothScroll wow fadeInUp" data-wow-delay="1.2s"><i
                              class="fa fa-angle-up"></i></a>
               </div>
          </div>
     </footer>