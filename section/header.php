<!DOCTYPE html>
<html lang="en">

<head>

     <title>บริษัท เฮลตี เวก้า จำกัด - ผลิตและจัดจำหน่ายหน้ากากอนามัยแอลกอฮอล์เจลล้างมือสินค้ารับประกันคุณภาพพร้อมบริการส่ง</title>

     <meta charset="UTF-8">
     <meta http-equiv="X-UA-Compatible" content="IE=Edge">
     <meta name="description" content="ผลิตและจัดจำหน่ายหน้ากากอนามัยแอลกอฮอล์เจลล้างมือสินค้ารับประกันคุณภาพพร้อมบริการส่งและบริการหลังการขายทุกกระบวนการผลิตของ บริษัท เฮลตี้เวก้า จำกัด ได้รับมาตรฐานสากลใส่ใจในทุกๆรายละเอียดมั่นใจได้ว่าลูกค้าได้ผลิตภัณฑ์ที่มีคุณภาพ">
     <meta name="keywords" content="ผลิตหน้ากากอนามัย,แอลกอฮอล์เจล,แอลกอฮอล์ล้างมือ,จำหน่ายหน้ากากอนามัย,โรงงานผลิตแอลกอฮอล์ล้างมือ">
     <meta name="author" content="cii3.net">
     <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
     <meta name="format-detection" content="telephone=no">

     <link rel="preload" as="style" href="css/bootstrap.min.css">
     <link rel="preload" as="style" href="css/font-awesome.min.css">
     <link rel="preload" as="font" type="font/woff2" crossorigin href="fonts/fontawesome-webfont.woff2?v=4.7.0">
     <link rel="preload" as="style" href="css/animate.css">
     <link rel="preload" as="style" href="css/owl.carousel.css">
     <link rel="preload" as="style" href="css/owl.theme.default.min.css">

     <link rel="stylesheet" href="css/bootstrap.min.css">
     <link rel="stylesheet" href="css/font-awesome.min.css">
     <link rel="stylesheet" href="css/animate.css">
     <link rel="stylesheet" href="css/owl.carousel.css">
     <link rel="stylesheet" href="css/owl.theme.default.min.css">

     <!-- MAIN CSS -->
     <link rel="preload" as="style" href="css/tooplate-style.css?v=<?php echo $time=time();?>">
     <link rel="stylesheet" href="css/tooplate-style.css?v=<?php echo $time;?>">

     <meta property="og:url"                content="<?php echo BASE_URL;?>" />
     <meta property="og:type"               content="article" />
     <meta property="og:title"              content="บริษัท เฮลตี เวก้า จำกัด - ผลิตและจัดจำหน่ายหน้ากากอนามัยแอลกอฮอล์เจลล้างมือสินค้ารับประกันคุณภาพพร้อมบริการส่ง" />
     <meta property="og:description"        content="ผลิตและจัดจำหน่ายหน้ากากอนามัยแอลกอฮอล์เจลล้างมือสินค้ารับประกันคุณภาพพร้อมบริการส่งและบริการหลังการขายทุกกระบวนการผลิตของ บริษัท เฮลตี้เวก้า จำกัด ได้รับมาตรฐานสากลใส่ใจในทุกๆรายละเอียดมั่นใจได้ว่าลูกค้าได้ผลิตภัณฑ์ที่มีคุณภาพ" />
     <meta property="og:image"              content="<?php echo BASE_URL;?>images/sharefb.jpg" />
     <meta property="og:image:width"              content="1200px" />
     <meta property="og:image:height"              content="630px" />
     <link rel="icon" 
      type="image/png" 
      href="<?php echo BASE_URL;?>images/favicon.png">


     <link rel="preload" as="script" href="js/jquery.js">
     <link rel="preload" as="script" href="js/bootstrap.min.js">
     <link rel="preload" as="script" href="js/jquery.sticky.js">
     <link rel="preload" as="script" href="js/jquery.stellar.min.js">
     <link rel="preload" as="script" href="js/wow.min.js">
     <link rel="preload" as="script" href="js/smoothscroll.js">
     <link rel="preload" as="script" href="js/owl.carousel.min.js">
     <link rel="preload" as="script" href="js/custom.js?v=<?php echo $time;?>">
     <script>
          const BASE_URL = '<?php echo BASE_URL;?>';
     </script>
</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

     <!-- PRE LOADER -->
     <section class="preloader">
          <div class="spinner">

               <span class="spinner-rotate"></span>

          </div>
     </section>
