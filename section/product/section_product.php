<?php
$ProductList = $get->getProductList(6);
?>
<div class="copyright-text text-left">
     <div class="container">
               <h3>PRODUCTS</h3>
     </div>
</div>
<!-- product -->
<section id="product-detail">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <!-- SECTION TITLE -->
                         <div class="section-title wow fadeInUp text-center" data-wow-delay="0.1s">
                              <h2>PRODUCTS WE SALE</h2>
                         </div>
                    </div>

                    <?php
                    $html = '';
                    foreach ($ProductList as $key => $value) {
                         $html .= '<div class="col-md-4 col-sm-6 mb-30">
                                   <!-- product THUMB -->
                                   <div class="product-thumb wow fadeInUp" data-wow-delay="0.4s">
                                        <a href="'.BASE_URL.'product-detail.html?id=' . $value->id. '">
                                             <img '.lazyload('upload/' . $value->cover, '360','266').' class="img-responsive" alt="' . $value->name. '">
                                        </a>
                                        <div class="product-info">
                                             <h3><a class="title-h3" href="'.BASE_URL.'product-detail.html">' . $value->name. '</a></h3>
                                             <p>' . $value->text_line1. '</p>
                                             <p>' . $value->text_line2. '</p>

                                             <a href="'.BASE_URL.'product-detail.html?id=' . $value->id. '" class="section-btn btn btn-default">SHOP NOW</a>
                                        </div>
                                   </div>
                              </div>';
                    }
                    echo $html;
                    ?>
                   
               </div>
          </div>
     </section>