<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
          <div class="container">

               <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                         <span class="icon icon-bar"></span>
                    </button>

                    <!-- lOGO TEXT HERE -->
                    <a href="<?php echo BASE_URL;?>" class="navbar-brand">
                         <img class="logo-web-cii3"
                              <?php echo lazyload('images/Web-logo.png','129');?>
                              alt="Healthy Vaga"
                              aria-label="Healthy Vaga">
                         <span class="hidden">Healthy Vaga</span>
                    </a>
               </div>

               <!-- MENU LINKS -->
               <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                         <li><a href="<?php echo BASE_URL;?>#about" class="smoothScroll">ABOUT US</a></li>
                         <li><a href="<?php echo BASE_URL;?>#team" class="smoothScroll">SERVICE</a></li>
                         <li><a href="<?php echo BASE_URL;?>product.html" class="smoothScroll">PRODUCT</a></li>
                         <li><a href="<?php echo BASE_URL;?>blog.html" class="smoothScroll">BLOG</a></li>
                         <li><a href="<?php echo BASE_URL;?>contact.html" class="smoothScroll">CONTACT</a></li>
                    </ul>
               </div>

          </div>
     </section>