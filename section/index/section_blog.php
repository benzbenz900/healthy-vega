<!-- NEWS -->
<?php
$getBlogList = $get->getBlogList(2);
?>
<section id="news">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <!-- SECTION TITLE -->
                         <div class="section-title wow fadeInUp" data-wow-delay="0.1s">
                              <h2>BLOG NEWS <a href="<?php echo BASE_URL;?>blog.html" class="open-link wow fadeInUp" data-wow-delay="0.3s"><i class="fa fa-external-link-square"></i> ดูทั้งหมด</a></h2>
                         </div>
                    </div>

                    <?php
                    $html = '';
                    foreach ($getBlogList as $key => $value) {
                         $html .= '<div class="col-md-6 col-sm-6 mb-30">
                         <!-- NEWS THUMB -->
                         <div class="news-thumb wow fadeInUp" data-wow-delay="0.4s">
                              <a href="'.BASE_URL.'blog-detail.html?id='.$value->id.'">
                                   <img  '.lazyload('upload/'.$value->cover,'555','370').' class="img-responsive" alt="'.$value->name.'">
                              </a>
                              <div class="news-info">
                                   <span>'.$get->DateThai($value->date_post).'</span>
                                   <h3><a class="title-h3" href="'.BASE_URL.'blog-detail.html?id='.$value->id.'">'.$value->name.'</a></h3>
                                   <p class="news-info-more">'.$value->description_short.'</p>
                                   <a href="'.BASE_URL.'blog-detail.html?id='.$value->id.'" class="section-btn btn btn-default show-now">READ MORE</a>
                              </div>
                         </div>
                    </div>';
                    }
                    echo $html;
                    ?>

               </div>
          </div>
     </section>