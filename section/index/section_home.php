<!-- HOME -->
<?php
$getBannerList = $get->getBannerList(3);
?>
<section id="home" class="slider">

     <div class="owl-carousel owl-theme wow fadeInUp" data-wow-delay="0.4s">

          <?php
          foreach ($getBannerList as $key => $value) {
               echo '
                    <div class="item" '.lazyload('upload/'.$value->image_bg, '1912', '650', 'style').'>
                         <div class="caption">
                              <div class="container">
                                   <div class="col-md-6">
                                        <h1>'.$value->name.'</h1>
                                        <h3>'.$value->description.'</h3>
                                        <a href="'.$value->link.'" class="section-btn btn btn-default smoothScroll">MORE INFO</a>
                                   </div>';
                              if($value->image_props != null){
                                   echo '<div class="col-md-6"><img class="img-responsive" '.lazyload('upload/'.$value->image_props).' alt="'.$value->name.'"></div>';
                              }
                            echo  '</div>
                         </div>
                    </div>
                    ';
          }
          ?>
          
     </div>


</section>