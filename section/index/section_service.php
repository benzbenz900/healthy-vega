<!-- TEAM -->
<section id="team">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <div class="about-info text-center">
                              <h2 class="wow fadeInUp" data-wow-delay="0.1s">OUR SERVICE</h2>
                         </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="col-md-4 col-sm-6 mb-30">
                         <div class="team-thumb bg1 wow fadeInUp" data-wow-delay="0.2s">
                              <div class="con-dorpsw">
                                   <div class="dorpsw"><img <?php echo lazyload('images/icon-Services-1.png','96');?>></div>
                              </div>
                              <h3>One Stop Service</h3>
                              <hr>
                              <p>รับผลิตและจัดจำหน่ายสินค้าหน้ากากอนามัยแอลกอฮอล์เจลล้างมือรับประกันคุณภาพพร้อมบริการส่งและบริการหลังการขาย</p>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-6 mb-30">
                         <div class="team-thumb bg2 wow fadeInUp" data-wow-delay="0.4s">
                              <div class="con-dorpsw">
                                   <div class="dorpsw"><img <?php echo lazyload('images/icon-Services-2.png','96');?>></div>
                              </div>
                              <h3>Quality Focus</h3>
                              <hr>
                              <p>ทุกกระบวนการผลิตของ บริษัท เฮลตี้เวก้า จำกัด ได้รับมาตรฐานสากลและใส่ใจในทุกๆรายละเอียดมั่นใจได้ว่าลูกค้าจะได้ผลิตภัณฑ์ที่มีคุณภาพ</p>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-6 mb-30">
                         <div class="team-thumb bg3 wow fadeInUp" data-wow-delay="0.6s">
                              <div class="con-dorpsw">
                                   <div class="dorpsw"><img <?php echo lazyload('images/icon-Services-3.png','96');?>></div>
                              </div>
                              <h3>Technology Driven</h3>
                              <hr>
                              <p>บริษัท เฮลที่เวก้า จำกัด พัฒนาและนำเทคโนโลยีใหม่ๆมาใช้เสมอๆเพื่อได้ผลิตภัณฑ์ที่มีคุณภาพ</p>
                         </div>
                    </div>

               </div>
          </div>
     </section>