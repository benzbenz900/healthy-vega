<!-- ABOUT -->
<section id="about" class="cii3_lazy" <?php echo lazyload('images/Web-2.png',null,'650','style');?>>
          <div class="container">
               <div class="row">
                   
                    <div class="col-md-6 col-sm-6">
                         <!-- <img class="img-responsive visible-xs-block" src="images/Web-2.png"> -->
                    </div>

                    <div class="col-md-6 col-sm-6">
                         <div class="about-info">
                              <h2 class="wow fadeInUp" data-wow-delay="0.1s">ABOUT US</h2>
                         </div>
     
                         <div class="clearfix"></div>

                         <div class="about-info">

                              <h2 class="wow fadeInUp title-h2" data-wow-delay="0.6s">บริษัท เฮลตี้เวก้า จำกัด</h2>
                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                   <p>ผลิตและจัดจำหน่ายหน้ากากอนามัยแอลกอฮอล์เจลล้างมือสินค้ารับประกันคุณภาพพร้อมบริการส่งและบริการหลังการขายทุกกระบวนการผลิตของ
                                        บริษัท เฮลตี้เวก้า จำกัด
                                        ได้รับมาตรฐานสากลใส่ใจในทุกๆรายละเอียดมั่นใจได้ว่าลูกค้าได้ผลิตภัณฑ์ที่มีคุณภาพ
                                   </p>
                              </div>

                         </div>
                    </div>

               </div>
          </div>
     </section>