<!-- FOOTER -->
<footer <?php echo lazyload('images/Contact-BG.png','1912','544','style');?>>
          <div class="container">
               <div class="row">

                    <div class="col-md-6 col-sm-12">
                         <div class="footer-thumb">
                              <img class="wow fadeInUp logo-web-cii3-footer" data-wow-delay="0.4s" <?php echo lazyload('images/Web-logo-ขาว.png','198');?> alt="Healthy Vaga"
                              aria-label="Healthy Vaga">
                              <h4 class="wow fadeInUp title-h1" data-wow-delay="0.6s">บริษัท เฮลตี เวก้า จำกัด</h4>

                              <div class="contact-info">
                                   <p class="wow fadeInUp" data-wow-delay="0.8s"><i class="fa fa-map-pin"></i> 298/1 หมู่ 5 ต.ปากเกร็ด อ.ปากเกร็ด จ.นนทบุรี 11120</p>
                                   <p class="wow fadeInUp" data-wow-delay="1s"><i class="fa fa-phone"></i> 092-496-4242</p>
                                   <p class="wow fadeInUp" data-wow-delay="1.2s"><i class="fa fa-envelope-o"></i> subpanich2520rich<i class="fa fa-at"></i>gmail.com</p>
                              </div>

                              <ul class="social-icon wow fadeInUp" data-wow-delay="1.4s">
                                   <li><a href="#" class="fa fa-facebook-square" attr="facebook icon"></a></li>
                                   <li><a href="#" class="fa fa-twitter"></a></li>
                                   <li><a href="#" class="fa fa-instagram"></a></li>
                              </ul>
                         </div>
                    </div>
               </div>
               <div class="angle-up-btn">
                    <a href="#top" class="smoothScroll wow fadeInUp" data-wow-delay="1.2s"><i
                              class="fa fa-angle-up"></i></a>
               </div>
          </div>
     </footer>