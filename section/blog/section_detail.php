<?php
 $id=(isset($_GET['id']) && is_numeric($_GET['id'])) ? $_GET['id'] : '1';
$getBlog = $get->getBlog($id);
$get->updateBlogView($id);
?>
<div class="copyright-text text-left">
     <div class="container">
               <h3><?php echo $getBlog->name;?></h3>
     </div>
</div>
<script type="application/ld+json">
{
  "@context": "https://schema.org",
  "@type": "BlogPosting",
  "mainEntityOfPage": {
    "@type": "WebPage",
    "@id": "<?php echo BASE_URL;?>blog-detail.html?id=<?php echo $id;?>"
  },
  "headline": "<?php echo $getBlog->name;?>",
  "description": "<?php echo $getBlog->name;?>",
  "image": "<?php echo BASE_URL.'upload/' . $getBlog->image_post;?>",  
  "author": {
    "@type": "Organization",
    "name": "บริษัท เฮลตี เวก้า จำกัด"
  },  
  "publisher": {
    "@type": "Organization",
    "name": "บริษัท เฮลตี เวก้า จำกัด",
    "logo": {
      "@type": "ImageObject",
      "url": "<?php echo BASE_URL;?>images/Web-logo.png",
      "width": 129,
      "height": 60
    }
  },
  "datePublished": "<?php echo $getBlog->view_hit;?>",
  "dateModified": "<?php echo $getBlog->view_hit;?>"
}
</script>
 <!-- NEWS DETAIL -->

 <section id="news-detail">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12">
                         <!-- SECTION TITLE -->
                         <div class="section-title wow fadeInUp text-center" data-wow-delay="0.1s">
                              <h2><?php echo $getBlog->name;?></h2>
                         </div>
                    </div>

                    <div class="col-md-8 col-sm-7">
                         <!-- NEWS THUMB -->
                         <div class="news-detail-thumb">
                              <div class="news-image">
                                   <img <?php echo lazyload('upload/' . $getBlog->image_post, '750'); ?> class="img-responsive" alt="<?php echo $getBlog->name;?>">
                              </div>
                              <h3><?php echo $getBlog->name;?></h3>
                              
                              <?php echo $getBlog->description;?>

                              <div class="news-social-share">
                                        <a href="#" class="btn btn-primary"><i class="fa fa-facebook"></i>Facebook</a>
                                        <a href="#" class="btn btn-success"><i class="fa fa-eye"></i><?php echo $get->DateThai($getBlog->view_hit);?></a>
                                        <a href="#" class="btn btn-danger"><i class="fa fa-calendar"></i><?php echo $get->DateThai($getBlog->date_post);?></a>
                              </div>
                         </div>
                    </div>

                    <div class="col-md-4 col-sm-5">
                         <div class="news-sidebar">
                              <!-- <div class="news-author">
                                   <h4>About the author</h4>
                                   <p>Lorem ipsum dolor sit amet, maecenas eget vestibulum justo imperdiet, wisi risus purus augue vulputate voluptate neque.</p>
                              </div> -->

                              <div class="fb-page" data-href="https://www.facebook.com/VegaCareMask" data-tabs="timeline" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/VegaCareMask" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/VegaCareMask">Vega Care หน้ากากอนามัย</a></blockquote></div>
                              <div id="fb-root"></div>
                                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v6.0&appId=2412722868776374&autoLogAppEvents=1"></script>

                              <div class="recent-post">
                                   <h4>Recent Posts</h4>
                                   <?php
                                   $getBlogList = $get->getBlogList(6);
                                   foreach ($getBlogList as $key => $valueBlogList) {
                                        echo '
                                        <div class="media">
                                             <div class="media-object pull-left">
                                                  <a href="'.BASE_URL.'blog-detail.html?id='.$valueBlogList->id.'"><img '.lazyload('upload/' . $valueBlogList->cover, '80').' class="img-responsive" alt="'.$valueBlogList->name.'"></a>
                                             </div>
                                             <div class="media-body">
                                                  <h4 class="media-heading"><a href="'.BASE_URL.'blog-detail.html?id='.$valueBlogList->id.'">'.$valueBlogList->name.'</a></h4>
                                             </div>
                                        </div>
                                        ';
                                   }
                                   ?>
                              </div>

                              <div class="news-tags">
                                   <h4>Tags</h4>
                                   <?php
                                   $tags = explode(',',$getBlog->tag_label);
                                   foreach ($tags as $key => $value) {
                                        echo '<li><a href="?id='.$_GET['id'].'&tag='.$value.'">'.$value.'</a></li>';
                                   }
                                   ?>
                              </div>
                         </div>
                    </div>
                    
               </div>
          </div>
     </section>