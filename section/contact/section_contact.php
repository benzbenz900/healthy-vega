<div class="copyright-text text-left">
     <div class="container">
               <h3>CONTACT US</h3>
     </div>
</div>


     <!-- MAKE AN APPOINTMENT -->
     <section id="appointment" data-stellar-background-ratio="3">
          <div class="container">
               <div class="row">

                    <div class="col-md-12 col-sm-12 text-center mb-30">
                         <h2 class="wow fadeInUp" data-wow-delay="0.6s">บริษัท เฮลตี เวก้า จำกัด</h2>

                         <div class="contact-info-company mb-30">
                              <p class="wow fadeInUp" data-wow-delay="0.8s"><i class="fa fa-map-pin"></i> 298/1 หมู่ 5 ต.ปากเกร็ด อ.ปากเกร็ด จ.นนทบุรี 11120</p>
                              <p class="wow fadeInUp" data-wow-delay="1s"><i class="fa fa-phone"></i> 092-496-4242</p>
                              <p class="wow fadeInUp mb-30" data-wow-delay="1.2s"><i class="fa fa-envelope-o"></i> subpanich2520rich<i class="fa fa-at"></i>gmail.com</p>
                         </div>

                    </div>

                    <div class="col-md-6 col-sm-6 mb-30 bg-ef">
                         <!-- CONTACT FORM HERE -->
                         <form id="appointment-form" role="form" method="post" action="#">

                              <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h3>แบบฟร์อมติดต่อเรา</h3>
                              </div>
                              
                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                   <div class="col-md-6 col-sm-6">
                                        <label for="name">ชื่อจริง</label>
                                        <input type="text" class="form-control" name="name" placeholder="กรอกชื่อจริง">
                                   </div>

                                   <div class="col-md-6 col-sm-6">
                                        <label for="subname">นามสกุลจริง</label>
                                        <input type="text" class="form-control" name="subname" placeholder="กรอกนามสกุลจริง">
                                   </div>

                                   <div class="col-md-6 col-sm-6">
                                        <label for="email">อีเมล์</label>
                                        <input type="email" class="form-control" name="email" placeholder="กรอกอีเมล์">
                                   </div>

                                   <div class="col-md-6 col-sm-6">
                                        <label for="telnumber">เบอร์ติดต่อ</label>
                                        <input type="text" class="form-control" name="telnumber" placeholder="กรอกเบอร์โทร">
                                   </div>
                                   
                                   <div class="col-md-12 col-sm-12">
                                        <label for="subject">เรื่องที่ติดต่อ</label>
                                        <input type="tel" class="form-control" name="subject" placeholder="หัวเรื่องที่ติดต่อ">
                                        <label for="Message">รายละเอียดที่ต้องการติดต่อ</label>
                                        <textarea class="form-control" rows="5" name="message" placeholder="รายละเอียดที่ต้องการติดต่อ"></textarea>
                                        <button type="submit" class="section-btn btn btn-default smoothScroll mb-30" name="submit">ส่งข้อความ</button>
                                   </div>
                              </div>
                        </form>
                    </div>

                    <div class="col-md-6 col-sm-6 mb-30">
                              <iframe  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3872.773697331775!2d100.4968634978076!3d13.912470594649475!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30e284f272fb914b%3A0x2e3c90a85a39a8d8!2z4LmA4LiX4Lio4Lia4Liy4Lil4LiZ4LiE4Lij4Lib4Liy4LiB4LmA4LiB4Lij4LmH4LiU!5e0!3m2!1sth!2sth!4v1584736419487!5m2!1sth!2sth" width="100%" height="560" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>

               </div>
          </div>
     </section>