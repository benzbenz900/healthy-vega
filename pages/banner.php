<div class="row">
    <div class="col-md-6">
        <?php
echo Xcrud::get_instance()
    ->hide_button('save_new')
    ->unset_view()
    ->hide_button('save_edit')
    ->hide_button('return')
    ->table('banner_web')
    ->change_type('description', 'textarea', '', 2000)
    ->label(
        array(
            'image_bg' => 'image_bg (1920x650)px',
        )
    )
    ->change_type('image_bg', 'image')
    ->change_type('image_props', 'image')
    ->field_tooltip('link', 'ใส่เป็น URL ของหน้าเว็บ หรือ Facebook เช่น https://www.facebook.com/')
;
?>
</div>
    <div class="col-md-6">
        <img src="images/admin_banner.jpg"  class="img-responsive">
    </div>
</div>
