<div class="row">
    <div class="col-md-6">
        <?php
        echo Xcrud::get_instance()
        ->unset_view()
        ->hide_button('save_edit')
        ->hide_button('save_new')
        ->hide_button('return')
        ->table('product_image_web')
        ->change_type('cover','image')
        ->change_type('A01','image')
        ->change_type('A02','image')
        ->change_type('A03','image')
        ->change_type('A04','image')
        ->change_type('A05','image')
        ;
        ?>
    </div>
    <div class="col-md-6">
        <img src="images/product_admin.jpg"  class="img-responsive">
        <img src="images/Web-product.jpg"  class="img-responsive">
    </div>
</div>
