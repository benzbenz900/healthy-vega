<div class="row">
    <div class="col-md-6">
        <?php
        echo Xcrud::get_instance()
        ->unset_view()
        ->hide_button('save_edit')
        ->hide_button('save_new')
        ->hide_button('return')
        ->table('blog_news')
        ->order_by('id','desc')
        ->columns('cover,name,date_post,view_hit')
        ->fields('cover,image_post,name,description_short,description,tag_label')
        ->change_type('cover','image')
        ->change_type('image_post','image')
        ->change_type('description','texteditor')
        ->change_type('description_short','textarea')
        ->field_tooltip('tag_label', 'ตัวอย่าง คำที่1,คำที่2,คำที่3')
        ->no_quotes('date_post')
        ->pass_var('date_post','now()', 'create')
        ->pass_var('view_hit',0, 'create')
        ;
        ?>
    </div>
    <div class="col-md-6">
        <img src="images/blog_admin_1.jpg"  class="img-responsive">
        <img src="images/blog_admin_2.jpg"  class="img-responsive">
    </div>
</div>
