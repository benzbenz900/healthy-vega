<!DOCTYPE HTML>
<html>
    <head>
    	<meta http-equiv="content-type" content="text/html;charset=utf-8" />  
    	<title><?php echo $title_1 ?> - ระบบจัดการข้อมูลเว็บ</title>
        <link href="assets/style.css" rel="stylesheet" type="text/css" />
        <link href="../xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    </head>
    
    <body>
        <div id="page">
            <div id="menu"><?php include(dirname(__FILE__).'/menu.php') ?></div>
            <div id="content">
                <div class="clr">&nbsp;</div>
                <h1><?php echo $title_1 ?></h1>
                <p><?php echo $description ?></p>
                <?php include($file) ?>
                <div class="clr">&nbsp;</div>
            </div>
        </div>
        <script src="../xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>