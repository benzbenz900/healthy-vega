<?php
$version = 'v1.6.26';
$pagedata = array(
    'default' => array(
        'title_1' => 'ผู้ติดต่อผ่านเว็บ',
        'description' => 'รายการด้านล่างนี้คือ ผู้ที่ส่งข้อมูลติดต่อผ่านช่องทางของเว็บไชต์',
        'filename' => 'contact.php',
    ),
    'blog' => array(
        'title_1' => 'บทความ Blog',
        'description' => 'บทความจะช่วยในเรื่องสร้างคำค้นหาเข้ามาในเว็บของคุณได้ คุณความเพิ่มบทความอย่างน้อย อาทิตย์ละ 2-3 บทความ ที่เกี่ยวกับสินค้าที่จำหน่าย',
        'filename' => 'blog.php',
    ),
    'banner' => array(
        'title_1' => 'แบนเนอร์หน้าแรก',
        'description' => 'แบนเนอร์หน้าแรก หรือรูป สไลด์ในหน้าแรก',
        'filename' => 'banner.php',
    ),
    'product' => array(
        'title_1' => 'สินค้า',
        'description' => 'รายการสินค้าของเว็บ โปรดดูภาพด้านข้างประกอบการแก้ไขรูปสินค้า',
        'filename' => 'product.php',
    ),
);
