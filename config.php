<?php
define('init_web', true);
define('BASE_URL', 'https://healthy-vega.com/');
define('DOMAIN_URL', 'healthy-vega.com/');
define('BASE_IMAGE', 'https://image.cii3.net/' . DOMAIN_URL);
define('webp', 'force_format=webp&q=50');
define('png', 'force_format=png');
define('jpeg', 'force_format=jpeg&q=80&optipress=3');
define('thumb', BASE_IMAGE . 'images/Web-logo.png?w=1&h=1&force_format=jpeg&q=80&optipress=3');

define('db_host', 'localhost');
define('db_name', 'vega_care');
define('db_username', 'vega_care');
define('db_password', 'care');

define('SEND_EMAIL_API_URL', 'https://api.mailgun.net/v3/new.lnwphp.in.th');
define('SEND_EMAIL_API_KEY', 'key-f44a1d9be3bc77272b08134fa5dd0aa2');

define('SEND_EMAIL_NAME', 'บริษัท เฮลตี เวก้า จำกัด');
define('SEND_EMAIL_REPLY', 'no-reply@lnwphp.in.th');

include_once 'libs/database.php';

function lazyload($image = '', $w = '', $h = '', $style = 'src')
{
    $html = ' loadimagecii3="true" ';
    if (is_numeric($h) && is_numeric($w)) {
        $html .= 'data-nwebp="' . BASE_IMAGE . $image . '?w=' . $w . '&h=' . $h . '&' . png . '" ';
        $html .= 'data-webp="' . BASE_IMAGE . $image . '?w=' . $w . '&h=' . $h . '&' . webp . '" ';
    } elseif (is_numeric($w)) {
        $html .= 'data-nwebp="' . BASE_IMAGE . $image . '?w=' . $w . '&' . png . '" ';
        $html .= 'data-webp="' . BASE_IMAGE . $image . '?w=' . $w . '&' . webp . '" ';
    } elseif (is_numeric($h)) {
        $html .= 'data-nwebp="' . BASE_IMAGE . $image . '?h=' . $h . '&' . png . '" ';
        $html .= 'data-webp="' . BASE_IMAGE . $image . '?h=' . $h . '&' . webp . '" ';
    } else {
        $html .= 'data-nwebp="' . BASE_IMAGE . $image . '?' . png . '" ';
        $html .= 'data-webp="' . BASE_IMAGE . $image . '?' . webp . '" ';
    }

    if ($style == 'src') {
        $html .= 'src="' . thumb . '" ';
    } else {
        $html .= 'style="background-image: url(\'' . thumb . '\');" ';
    }
    return $html;
}

class loadDB
{
    private $db;
    public function __construct()
    {
        $this->db = new database();
    }

    public function getBannerList($limit = 3)
    {
        return $this->db->table('banner_web')->order_by('id DESC')->limit($limit)->find();
    }

    public function getProduct($id = '1')
    {
        return $this->db->table('product_image_web')->order_by('id DESC')->limit(1)->find('id', $id)[0];
    }

    public function getProductList($limit = 2)
    {
        return $this->db->table('product_image_web')->order_by('id DESC')->limit($limit)->find();
    }

    public function getBlog($id = '1')
    {
        return $this->db->table('blog_news')->order_by('id DESC')->limit(1)->find('id', $id)[0];
    }

    public function savePostContact()
    {
        if (!empty($_POST['name']) && !empty($_POST['subname']) && !empty($_POST['email']) && !empty($_POST['telnumber']) && !empty($_POST['subject']) && !empty($_POST['message'])) {
            $savePostContact = $this->db->table('contact_web')->create();
            $savePostContact->set('name', $_POST['name']);
            $savePostContact->set('subname', $_POST['subname']);
            $savePostContact->set('email', $_POST['email']);
            $savePostContact->set('telnumber', $_POST['telnumber']);
            $savePostContact->set('subject', $_POST['subject']);
            $savePostContact->set('message', $_POST['message']);
            $id = $savePostContact->save();
            if(is_numeric($id)){
                $this->sendmail($_POST['email'], 'เฮลตี เวก้า ได้รับข้อความของคุณเรียบร้อย', '
                ชื่อผู้ติดต่อ: '.$_POST['name'].' '.$_POST['subname'].'<br>
                อีเมล์: '.$_POST['email'].'<br>
                เบอร์ติดต่อ: '.$_POST['telnumber'].'<br>
                เรื่อง: '.$_POST['subject'].'<br>
                ข้อความ: '.$_POST['message'].'<br><br><br>
                ');

                $this->sendmail('subpanich2520richgmail.com', 'มีคนส่งข้อความผ่านเว็บ ถึง เฮลตี เวก้า', '
                ชื่อผู้ติดต่อ: '.$_POST['name'].' '.$_POST['subname'].'<br>
                อีเมล์: '.$_POST['email'].'<br>
                เบอร์ติดต่อ: '.$_POST['telnumber'].'<br>
                เรื่อง: '.$_POST['subject'].'<br>
                ข้อความ: '.$_POST['message'].'<br><br><br>
                ');
            }else{
                echo 'error';
            }
        } else {
            echo 'error';
        }
    }

    public function updateBlogView($id = '1')
    {
        $updateBlogView = $this->db->table('blog_news')->order_by('id DESC')->limit(1)->find_one('id', $id);
        $updateBlogView->set('view_hit', $updateBlogView->view_hit + 1);
        $updateBlogView->save();
    }

    public function getBlogList($limit = 2)
    {
        return $this->db->table('blog_news')->order_by('id DESC')->limit($limit)->find();
    }

    public function DateThai($strDate)
    {
        $strYear = date("Y", strtotime($strDate)) + 543;
        $strMonth = date("n", strtotime($strDate));
        $strDay = date("j", strtotime($strDate));
        $strMonthCut = array("", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค.");
        $strMonthThai = $strMonthCut[$strMonth];
        return "$strDay $strMonthThai $strYear";
    }

    public function mailgunsend($arr)
    {
        $html = file_get_contents('email/index.html');
        $html = str_replace(array('{SUBJECT}', '{MESSAGE}', '{BASE_URL}'), array($arr['subject'], $arr['html'], BASE_URL), $html);

        $array_data = array(
            'from' => SEND_EMAIL_NAME . ' <' . SEND_EMAIL_REPLY . '>',
            'to' => $arr['to'],
            'subject' => $arr['subject'],
            'html' => $html,
            'text' => $arr['text'],
            'o:tracking' => 'yes',
            'o:tracking-clicks' => 'yes',
            'o:tracking-opens' => 'yes',
            'o:tag' => 'yes',
            'h:Reply-To' => '<' . SEND_EMAIL_REPLY . '>',
        );
        $session = curl_init(SEND_EMAIL_API_URL . '/messages');
        curl_setopt($session, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($session, CURLOPT_USERPWD, 'api:' . SEND_EMAIL_API_KEY);
        curl_setopt($session, CURLOPT_POST, true);
        curl_setopt($session, CURLOPT_POSTFIELDS, $array_data);
        curl_setopt($session, CURLOPT_HEADER, false);
        curl_setopt($session, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($session, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($session, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($session);
        curl_close($session);
        $results = json_decode($response, true);
        return $results;
    }

    public function sendmail($tomail, $subject, $messages)
    {
        $arr = array('to' => $tomail,
            'subject' => $subject,
            'html' => $messages,
            'text' => '',
        );

        $this->mailgunsend($arr);
    }

}
$get = new loadDB();
