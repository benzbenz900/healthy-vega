(function ($) {

  "use strict";

  // PRE LOADER
  $(window).load(function () {
    $('.preloader').fadeOut(500); // set duration in brackets    
  });


  //Navigation Section
  $('.navbar-collapse a').on('click', function () {
    $(".navbar-collapse").collapse('hide');
  });


  // Owl Carousel
  $('.owl-carousel').owlCarousel({
    animateOut: 'fadeOut',
    items: 1,
    loop: false,
    dots: false,
    autoplay: true,
  })


  // PARALLAX EFFECT
  // $.stellar();  


  // SMOOTHSCROLL
  $(function () {
    $('.navbar-default a, #home a, footer a').on('click', function (event) {
      var $anchor = $(this);
      $('html, body').stop().animate({
        scrollTop: $($anchor.attr('href')).offset().top - 49
      }, 1000);
      event.preventDefault();
    });
  });

  function canUseWebP() {
    var elem = document.createElement('canvas');

    if (!!(elem.getContext && elem.getContext('2d'))) {
      // was able or not to get WebP representation
      return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
    }

    // very old browser like IE 8, canvas not supported
    return false;
  }

  if (canUseWebP()) {
    new LazyLoad({
      elements_selector: "[loadimagecii3='true']",
      data_src: 'webp'
    });
  } else {

    new LazyLoad({
      elements_selector: "[loadimagecii3='true']",
      data_src: 'nwebp'
    });
  }
  var click_one = 0;
  $('#appointment-form').on('submit', function (e) {
    e.preventDefault();
    if (click_one == 0) {

      var r = confirm("ยืนยันในการส่งข้อมูลการติดต่อ");
      if (r == true) {
        fetch(`${BASE_URL}rest.html`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          body: $(this).serialize()
        }).then(e => {
          return e.text()
        }).then(e => {
          console.log(e)
          if(e=='error'){
            alert('!!! ส่งข้อความไม่สำเร็จ กรุณาโหลดหน้านี้ใหม่ แล้วลองอีกครั้ง')
          }else{
            var r = confirm("ยืนยันในการส่งข้อมูลการติดต่อ");
            if (r == true) {
              location.reload()
            }else{
              location.reload()
            }
          }
        })
      }

    } else {
      alert('กำลังส่งข้อความ...')
    }
  })

  // WOW ANIMATION
  // new WOW({ mobile: false }).init();

})(jQuery);
