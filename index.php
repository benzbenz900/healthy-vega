<?php
include_once "config.php";
include_once "section/header.php";
include_once "section/index/nav_index.php";
include_once "section/index/section_home.php";
include_once "section/index/section_about.php";
include_once "section/index/section_service.php";
include_once "section/index/section_product.php";
include_once "section/index/section_blog.php";
include_once "section/index/footer_index.php";
include_once "section/footer.php";
?>